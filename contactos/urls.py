from django.urls import include, path
from rest_framework import routers

from . import views


router = routers.DefaultRouter()
router.register('contactos', views.ContactoViewSet)
router.register('tareas', views.TareaViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
