from django.db import models


class Contacto(models.Model):
    nombre = models.CharField('Nombre', max_length=100)
    apellidos = models.CharField('Apellidos', max_length=100)
    telefono = models.CharField('Teléfono', max_length=10)

    class Meta:
        verbose_name = 'contacto'
        verbose_name_plural = 'contactos'
        ordering = ['nombre', 'apellidos']

    def __str__(self):
        return '{} {}'.format(self.nombre, self.apellidos).strip()


class Tarea(models.Model):
    fecha_creacion = models.DateTimeField('Fecha de creación',
        auto_now_add=True)
    descripcion = models.CharField('Descripción', max_length=200)
    terminada = models.BooleanField('Terminada', default=False)

    class Meta:
        verbose_name = 'tarea'
        verbose_name_plural = 'tareas'
        ordering = ['fecha_creacion']

    def __str__(self):
        return self.descripcion
