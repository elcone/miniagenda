from rest_framework import serializers

from . import models


class ContactoSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Contacto
        fields = '__all__'


class TareaSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Tarea
        fields = '__all__'
